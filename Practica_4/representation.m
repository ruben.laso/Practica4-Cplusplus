function [  ] = representation( mesh_file, a_file, b_file, results_file )
    points = dlmread(mesh_file);
    A = dlmread(a_file);
    B = dlmread(b_file);
    results = dlmread(results_file);

    x = points(:,1);
    y = points(:,2);

    close all

    tri = delaunay(x,y);
    figure(1)
    trisurf(tri, x, y, results);
    axis([-1 1 -1 1 min(results)-1 max(results)+1])
    view(0,90)
    colorbar
    colormap(hot)

    min_cplusplus = min(results)
    max_cplusplus = max(results)

    u = @(x,y) (x.^2 - 1) .* (y.^2 - 1) .* (x.^2 + y.^2 - 1/4);
    results_analytic = u(x,y);
    figure(2);
    trisurf(tri, x, y, results_analytic);
    axis([-1 1 -1 1 min(results_analytic)-1 max(results_analytic)+1])
    view(0,90)
    colorbar
    colormap(hot)

    min_analytic = min(results_analytic)
    max_analytic = max(results_analytic)

    max_error = max(abs(results - results_analytic))
    rel_error = norm(results - results_analytic) / norm(results)
end  % representation
