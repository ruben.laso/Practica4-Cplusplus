#include <point.h>
#include <cmath>

void point::print_point() const {
    cout << "(" << this->x << "," << this->y << ")" << endl;
}

void point::print_point_extended() const {
    std::cout << "Point " << this->ind << " (frontier " << this->front << "): coord(" << this->x << "," << this->y << "), solution(" << this->sol << ")" << std::endl;
}

// void point::print_point() {
//     std::cout << "Point " << ind << " (frontier " << front << "): coord(" << x << "," << y << "), solution(" << sol << ")" << std::endl;
// }

double point::prod_escalar(const point p) const {
    return (this->x * p.x) + (this->y * p.y);
}

double point::euclidean_distance(const point p) const {
    return sqrt(pow(this->x - p.x, 2) + pow(this->y - p.y, 2));
}

// std::ostream & operator<<(std::ostream & stream, const point & p) {
//     stream << "Point " << p.ind << " (frontier " << p.front << "): coord(" << p.x << "," << p.y << "), solution(" << p.sol << ")";
//     return stream;
// }

std::ostream & operator<<(std::ostream & stream, const point & p) {
    stream << "(" << p.x << "," << p.y << ")";
    return stream;
}
