#include "pelfin.h"

#include "point.h"
#include <cmath>

P1::P1() {
    this->N=3;
    this->V=new point*[3];
}

void P1::print_finel() const {
    std::cout << "---------------" << std::endl;
    for (int k = 0; k < (this->N); k++){
        this->V[k]->print_point();
    }
    std::cout << "---------------" << std::endl;
}

void P1::calcula_area() {
    this->area = abs(0.5 * (this->V[1]->x * this->V[2]->y +
        this->V[0]->x * this->V[1]->y +
        this->V[2]->x * this->V[0]->y -
        this->V[1]->x * this->V[0]->y -
        this->V[0]->x * this->V[2]->y -
        this->V[2]->x * this->V[1]->y));
}

void P1::calcula_gradientes() {
    for (int i = 0; i < N; i++) {
        // point pi = *this->V[i];
        point pi1 = *this->V[(i+1)%N];
        point pi2 = *this->V[(i+2)%N];

        gradientes[i].x = 1/(2.0 * this->area) * (-(pi2.y - pi1.y));
        gradientes[i].y = 1/(2.0 * this->area) * (pi2.x - pi1.x);
    }
}

void P1::calcula_baricentro() {
    this->baricentro.x = (this->V[0]->x + this->V[1]->x + this->V[2]->x) / 3.0;
    this->baricentro.y = (this->V[0]->y + this->V[1]->y + this->V[2]->y) / 3.0;
}

void P1::calcula_atributos() {
    this->calcula_area();
    this->calcula_gradientes();
    this->calcula_baricentro();
}

double P1::calcula_matriz_local(const uint i, const uint j) const {
    int idx1 = -1;
    int idx2 = -1;

    for (int ii = 0; ii < N; ii++) {
        if (this->V[ii]->ind == i) {
            idx1 = ii;
        }
        if (this->V[ii]->ind == j) {
            idx2 = ii;
        }
    }

    if (idx1 == -1 || idx2 == -1) {
        return 0;
    } else if (this->V[idx1]->front == this->V[idx1]->FRONT_DIRICHLET || this->V[idx2]->front == this->V[idx2]->FRONT_DIRICHLET) {
        return 0;
    } else {
        return this->area * this->gradientes[idx1].prod_escalar(this->gradientes[idx2]);
    }
}

double P1::calcula_funcion_base(const uint i, const point p) const {
    int idx = -1;

    for (int ii = 0; ii < N; ii++) {
        if (this->V[ii]->ind == i) {
            idx = ii;
            break;
        }
    }

    int idx1 = (idx + 1) % 3;
    int idx2 = (idx + 2) % 3;

    if (idx == -1) {
        return 0;
    } else if (p.front == p.FRONT_DIRICHLET) {
        return 0;
    } else {
        return 1/(2.0*this->area) * (this->V[idx1]->x * this->V[idx2]->y
            + p.x * this->V[idx1]->y
            + this->V[idx2]->x * p.y
            - this->V[idx1]->x * p.y
            - p.x * this->V[idx2]->y
            - this->V[idx2]->x * this->V[idx1]->y);
    }
}
