#include "header.h"
#include "mesh.h"
#include "dato.h"
#include <stdlib.h>
// #include "../../borrar/duda_Maria_9Marzo/Malla.h"

int main(int argc, char const *argv[]) {

   dato datos;
   datos.leedatos();

   Mfinel<P1> Malla; // Elijo EN ESTE MOMENTO el tipo de Elemento finito, P1

   Malla.fill_mesh(datos); // lee los mallados desde fichero

   Malla.construye_matriz_global(); // Construimos la matriz global del sistema y el termino independiente

   Malla.print_nodes(); // Imprimimos los nodos
   Malla.print_elements(); // Imprimimos los elementos

   Malla.solve();

   return 0;
}
