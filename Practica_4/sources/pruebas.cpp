#include <iostream>
#include <cstdlib>

#include "point.h"
#include "pelfin.h"
#include "dato.h"

int main(int argc, char const *argv[]) {
    point p1;
    p1.x = 3; p1.y = 2; p1.ind = 0; p1.front = p1.INNER_POINT; p1.sol = 0;

    std::cout << "Point p1: "; p1.print_point();
    std::cout << "Point p1: "; p1.print_point_extended();

    point p2;
    p2.x = 4; p2.y = -1; p2.ind = 1; p2.front = p2.FRONT_DIRICHLET; p2.sol = -1;
    std::cout << "Point p2: "; p2.print_point();
    std::cout << "Point p2: "; p2.print_point_extended();

    std::cout << "Escalar product p1*p2 = " << p1 << "*" << p2 <<" = " << p1.prod_escalar(p2) << std::endl;
    std::cout << "Euclidean distance between p1 and p2 = " << p1.euclidean_distance(p2) << std::endl;

    std::cout << "\n\n" << std::endl;

    point p3;
    p3.x = 2; p3.y = 0; p3.ind = 2; p3.front = p3.INNER_POINT; p3.sol = 0;

    P1 el1;
    el1.V[0] = &p1; el1.V[1] = &p2; el1.V[2] = &p3;

    std::cout << "Printing el1..." << std::endl;
    el1.print_finel();
    el1.calcula_atributos();
    std::cout << "Baricenter of el1: " << el1.baricentro << std::endl;
    std::cout << "Area of el1: " << el1.area << std::endl;
    for (int i = 0; i < el1.N; i++) {
        std::cout << "Gradient " << i << " of el1: " << el1.gradientes[i] << std::endl;
    }
    for (uint i = 0; i < (uint) el1.N; i++) {
        for (uint j = 0; j < (uint) el1.N; j++) {
            std::cout << "Matrix element [" << i << "," << j <<"]: " << el1.calcula_matriz_local(i, j) << std::endl;
        }
    }
    std::cout << "Matrix element [" << 0 << "," << 35 <<"]: " << el1.calcula_matriz_local(0, 35) << std::endl;
    std::cout << "Matrix element [" << 20 << "," << 0 <<"]: " << el1.calcula_matriz_local(20, 0) << std::endl;
    std::cout << "Matrix element [" << 20 << "," << 35 <<"]: " << el1.calcula_matriz_local(20, 35) << std::endl;

    std::cout << "Computing B_0 = int_{el1} f(x,y) * phi_i(x,y) dxdy = " << int_f(el1, 0) << '\n';
    std::cout << "Computing B_1 = int_{el1} f(x,y) * phi_i(x,y) dxdy = " << int_f(el1, 1) << '\n';
    std::cout << "Computing B_2 = int_{el1} f(x,y) * phi_i(x,y) dxdy = " << int_f(el1, 2) << '\n';
    std::cout << "Computing B_35 = int_{el1} f(x,y) * phi_i(x,y) dxdy = " << int_f(el1, 35) << '\n';

    std::cout << '\n';
    for (size_t i = 0; i < 3; i++) {
        std::cout << "Computing base function for el1, in the baricenter and in node " << i << ": " << el1.calcula_funcion_base(i, el1.baricentro) << '\n';
    }
    std::cout << "Computing base function for el1, in the baricenter and in node " << 35 << ": " << el1.calcula_funcion_base(35, el1.baricentro) << '\n';

    return EXIT_SUCCESS;
}
