#ifndef __DATO_
#define __DATO_

#include "header.h"
#include "point.h"
#include "pelfin.h"
#include <cmath>

struct fich_mesh {
    string nodos;
    string conect;
    string boundary; // COMPLETAR!!
};

class dato {
    public:
        fich_mesh mallado;
        void leedatos();
        double f(const point p);
};


/*
 * Lectura de fichero de configuracion de la simulacion
 * En este caso solo leemos los ficheros que almacenan la malla
 */
void dato::leedatos() {
    ifstream fichdatos ("datos.dat");
    if (fichdatos.is_open()) {
        fichdatos>>this->mallado.nodos;
        cout<<"Fichero de nodos: "<<this->mallado.nodos<<endl;
        fichdatos>>this->mallado.conect;
        fichdatos>>this->mallado.boundary;
    }
}


double f(const point p) {
    return (-1.0) * (2.0*pow(p.x,4.0) + pow(p.x,2.0)*24.0*pow(p.y,2.0) - pow(p.x,2)*33.0/2.0 + 2*pow(p.y,4) - 33.0/2.0*pow(p.y,2.0) + 5.0);
}

double int_f(const P1 t, const int i) {
    return t.area * t.calcula_funcion_base(i, t.baricentro) * f(t.baricentro);
}

#endif
