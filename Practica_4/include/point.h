#ifndef __POINT_
#define __POINT_

#include <iostream>
#include <cstdlib>
#include <list>
using namespace std;

class point{
    public:
        double x;
        double y;
        uint ind;
        uint front;
        double sol;
        void print_point() const;
        void print_point_extended() const;
        double prod_escalar(const point p) const;
        static const uint INNER_POINT = 0;
        static const uint FRONT_DIRICHLET = 1;
        double euclidean_distance(const point p) const;
};

std::ostream & operator<<(std::ostream & stream, const point & p);

#endif
