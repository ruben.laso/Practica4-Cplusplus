/*
 * Elemento finito P1
 */

#ifndef __P1_
#define __P1_

#include "point.h"

class P1{
    private:
        void calcula_area();
        void calcula_gradientes();
        void calcula_baricentro();
    public:
        // Atributos
        int N;      // numero de vertices
        point** V;  // puntero de puntero a vertices del el. finito: contiene tres punteros
		            // que guardan la direccion de memoria de los vertices, ya creados
        double area;
        point gradientes[3];  // se asume que un elemento tiene tres vertices, dado el enunciado de la practica
                              // al ser los gradientes constantes, podemos almacenar el valor
                              // para no repetir los calculos.
        point baricentro;
        // Metodos de la clase Q1
        P1();
        void calcula_atributos(); // calcula los atributos: gradientes, baricentro y area del triangulo
        void print_finel() const;
        double calcula_matriz_local(const uint i, const uint j) const;
        double calcula_funcion_base(const uint i, const point p) const;
};

#endif
