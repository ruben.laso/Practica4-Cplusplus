/*
 * Elemento finito Q1
 */

#ifndef __Q1_
#define __Q1_

class Q1{
    public:
        // Atributos
        int N;        // numero de vertives
        point *V;    // puntero a vertices del el finito
        // Metodos de la clase Q1
        Q1();
        void print_finel();
};

Q1::Q1(){
    this->N=4;
    this->V=new point[4];
//  this->V=NULL;
}

void Q1::print_finel(){
    for (int k=0;k<(this->N);k++){
        this->V[k].print_point();
    }
}

#endif
