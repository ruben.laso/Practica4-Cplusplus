/*
 *
 *          CLASE MALLA
 *
 *
 * Clase malla generica con templates
 *
 * Lnode: Vector de nodos del mallado
 * Lfinel: Lista de elementos finitos (poligonos) del mallado
 *
 *
 */


/*
 * Una malla de elementos finitos es una lista de elementos finitos T
 * FIJATE: T puede ser cualquier cosa: cuadrados Q1,  triangulos P1, etc
 *
 * Lo bueno de usar una lista es que la podemos modificar facilmente: añadir, eliminar elementos, etc
 * MUY adecuado para hacer refinamiento dinamico de mallados
 */
#include "header.h"
#include <list>
#include <vector>
#include "point.h"
// #include "qelfin.h" // Si quisiesemos usar Q1
#include "pelfin.h"
#include "dato.h"
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;


class dato;

template < typename T >
class Mfinel{


public :
    friend class dato;
    static int Nnodes;  // Num de nodos
    static int Nfinel; //Num de els finitos
    vector<point*> Lnode; // lista de  nodos
    list<T> Lfinel; // lista de elementos
    MatrixXd assembly; // matriz de ensamblaje
    VectorXd ind_term; // termino independiente

    // Metodos
    Mfinel(); // constructor por defecto
    void fill_mesh(dato& datos); // rellena la malla
    void construye_matriz_global();
    void print_nodes();
    void print_elements();
    void solve();
};


template < typename T >
Mfinel<T>::Mfinel(){
  vector<point*> Lnode;
}


/*
 * Definicion de miebros estaticos
 */
template <typename T> int Mfinel<T>::Nnodes;
template <typename T> int Mfinel<T>::Nfinel;

/*
 * Un metodo que rellena la malla a partir de datos provenientes
 * de un fichero generado por un mallador o a partir de nuestro propio código de mallado
 *
 * El nombre de fichero debería estar almacenado en una estructura datos del problema
 *
 *
 */
template < typename T >
void Mfinel<T>::fill_mesh(dato& datos) {

    //  Rellenamos el vector de nodos
    this->Nnodes=0;
    ifstream myfile (datos.mallado.nodos.c_str()); // ojo!: es necesario convertir el string a pointer de char con c_str
    if (myfile.is_open()) {
        while ( myfile.good() ) {
            point *node = new point();              // creo nuevo nodo
            myfile >> node->x >> node->y;  // asigno coordenadas
            node->ind = this->Nnodes++; // incrementamos el numero de nodos
            // cout<<node.x<<" "<<node.y<<endl; // Imprimir a medida que leemos
            // cout << "numero de nodos " << this->Nnodes << endl;
            this->Lnode.push_back(node); // insertamos nodo en lista-nodos
        }
        myfile.close();
    }

    // Marcamos los nodos frontera
    int ind;
    ifstream myfile2 (datos.mallado.boundary.c_str()); // ojo!: es necesario convertir el string a pointer de char con c_str
    if (myfile2.is_open()) {
        typename list<T>::iterator it;
        while ( myfile2.good() ) {
            myfile2 >> ind;
            this->Lnode[ind-1]->front = point::FRONT_DIRICHLET;
        }
        myfile.close();
    }

    this->Nfinel=0.;

    // Rellenamos la lista de elementos

    myfile.open("mesh/mesh_tnode.dat");

    int conect;
    if (myfile.is_open()) {
        while (myfile.good()) {
            T Elmt=T();  // creamos nuevo elemento temporal
            for(int k=0; k < Elmt.N; k++){ // Leemos conectividades
                // cout << "insertamos el elemento"<<endl;
        	    myfile >> conect;
        	    // cout<<"Conectividad"<<conect<<endl;
                Elmt.V[k] = this->Lnode[conect-1]; // asigno REFERENCIA al punto correspondiente
        	    // cout << "Fijaos!! Mismas Dir. de Memoria (NO COPIAMOS el contenido del punto) " << (Elmt.V[k]) << " " << (this->Lnode[conect]) << endl;
        	    this->Nfinel++;
            }
            Elmt.calcula_atributos();
            this->Lfinel.push_back(Elmt); // insertamos el elemento en la lista de elementos
            // cout << endl;
        }
        myfile.close();
    }
}

/*
 * Un metodo que construye la matriz de ensamblaje.
 */
template < typename T >
void Mfinel<T>::construye_matriz_global() {
    this->assembly = MatrixXd(Nnodes, Nnodes);
    this->ind_term = VectorXd(Nnodes);
    typename list<T>::iterator it;
    // Initialize elements to zero, to avoid incorrect results
    // Matrices and vectors are not always initialized to zero when creating the instances
    for (int i = 0; i < this->assembly.rows(); i++) {
        for (int j = 0; j < this->assembly.cols(); j++) {
            this->assembly(i,j) = 0;
        }
    }
    //Compute global matrix elements
    int ii, jj;
    for (it = this->Lfinel.begin(); it != this->Lfinel.end(); ++it) {
        for (int i = 0; i < it->N; i++) {
            ii = it->V[i]->ind;
            for (int j = 0; j < it->N; j++) {
                jj = it->V[j]->ind;
                this->assembly(ii,jj) += it->calcula_matriz_local(ii,jj);
            }
        }
    }

    // Initialize elements to zero, to avoid incorrect results
    // Matrices and vectors are not always initialized to zero when creating the instances
    for (int i = 0; i < this->ind_term.size(); i++) {
        this->ind_term(i) = 0;
    }
    // Compute independent term elements
    for (it = this->Lfinel.begin(); it != this->Lfinel.end(); ++it) {
        for (int j = 0; j < it->N; j++) {
            jj = it->V[j]->ind;
            this->ind_term(jj) += int_f(*it, jj);
        }
    }
}

// Impresion de lista de nodos de la malla
template < typename T >
void Mfinel<T>::print_nodes() {
    cout << "Imprimimos los puntos desde la lista del mallado" << endl;
    vector<point*>::iterator it; // iterator de lista de nodos
    for(it = this->Lnode.begin(); it != this->Lnode.end(); ++it) {
        (*it)->print_point(); // alternativamente  (*(*it)).print_points();
    }
}


// Impresion de lista de nodos de la malla
template < typename T >
void Mfinel<T>::print_elements() {
    cout<<"Imprimimos los elementos del mallado"<<endl;
    int i=0;
    // list<T>::iterator it; // MAL: iterator de lista de els. No conoce T
    typename list<T>::iterator it; // cuidado con el typename- si no lo ponemos no se entera en el iterador
    for(it = this->Lfinel.begin(); it != this->Lfinel.end(); ++it) {
        cout << "ELEMENTO " << i << endl;
        it->print_finel(); // alternativamente  (*(*it)).print_points();
        i++;
    }
}

template < typename T >
void Mfinel<T>::solve() {
    cout << "Here is the matrix A:\n" << this->assembly << endl;
    cout << "Here is the vector b:\n" << this->ind_term << endl;
    VectorXd x = this->assembly.colPivHouseholderQr().solve(this->ind_term);
    cout << "The solution is:\n" << x << endl;

    double relative_error = (this->assembly*x - this->ind_term).norm() / this->ind_term.norm(); // norm() is L2 norm
    cout << "The relative error is:\n" << relative_error << endl;

    // save results in the nodes
    for(int i = 0; i < this->Nnodes; ++i) {
        this->Lnode[i]->sol = x(i);
    }

    ofstream myfile;
    myfile.open ("result.txt");
    myfile << x << endl;
    myfile.close();

    myfile.open ("A.txt");
    myfile << this->assembly << endl;
    myfile.close();

    myfile.open ("B.txt");
    myfile << this->ind_term << endl;
    myfile.close();
}

// template < typename T >
// void Mfinel<T>::solve(){
//     Matrix3f A;
//     Vector3f b;
//     A << 1,2,3,  4,5,6,  7,8,10;
//     b << 3, 3, 4;
//     cout << "Here is the matrix A:\n" << A << endl;
//     cout << "Here is the vector b:\n" << b << endl;
//     Vector3f x = A.colPivHouseholderQr().solve(b);
//     cout << "The solution is:\n" << x << endl;
// }
